package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository <E extends AbstractEntity>{
    protected List<E> items = new ArrayList<>();
    protected Map<String, E> itemHashMap = new HashMap<>();

    protected Map<String, E> storage;
    protected final String fileName;

    public AbstractRepository(String fileName) {
        this.fileName = fileName;
        storage = new HashMap<>();
    }


    public void create(E value) {
        items.add(value);
        itemHashMap.putIfAbsent(value.getName(), value);

        storage.putIfAbsent(value.getName(), value);
        saveStorage();
    }

    public E update(final Long id, final String name) {
        final E result = findById(id);
        if (result == null) return null;
        result.setId(id);
        result.setName(name);
        return result;
    }

    public E update(final Long id, final String name, final String description) {
        final E result = findById(id);
        if (result == null) return null;
        result.setId(id);
        result.setName(name);
        result.setDescription(description);
        return result;
    }

    public E findByIndex(int index) {
        return items.get(index);
    }

    public E removeByIndex(final int index) {
        final E result = findByIndex(index);
        if (result == null) return null;
        items.remove(result);
        itemHashMap.remove(result.getName());
        return result;
    }

    public E findByName(final String name) {
        if (itemHashMap.containsKey(name)) {
            AbstractEntity result = itemHashMap.get(name);
            return (E) result;
        }
        else return null;
    }

    public E removeByName(final String name) {
        final E result = findByName(name);
        if (result == null) return null;
        items.remove(result);
        itemHashMap.remove(result.getName());
        return result;
    }

    public E findById(final Long id) {
        E result = null;
        for (final E item : items) {
            if (id.equals(item.getId())) {
                result = item;
                break;
            }
        }
        return result;
    }

    public E removeById(final Long id) {
        final E result = findById(id);
        items.remove(result);
        itemHashMap.remove(result.getName());
        return result;
    }

    public List<E> findAllByUserId(final Long userId) {
        final List<E> items = new ArrayList<>();
        for (final E result : findAll()) {
            final Long idUser = result.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) items.add((E) result);
        }
        return items;
    }

    public void clear() {
        items.clear();
    }

    public List<E> findAll() {
        return items;
        //return new ArrayList<>(storage.values());
    }

    public int getSize() {
        return items.size();
    }

    public E findByUserIdAndId(final Long userId, final Long id) {
        for (final E result : items) {
            final Long idUser = result.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (result.getId().equals(id)) return result;
        }
        return null;
    }

    protected abstract void saveStorage();

    protected abstract void updateStorage();
}
