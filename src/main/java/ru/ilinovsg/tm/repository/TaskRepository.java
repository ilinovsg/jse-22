package ru.ilinovsg.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.ilinovsg.tm.entity.Task;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task>{
    private final File file = new File("task.save");

    private TaskRepository() {
        this("task.save");
    }

    public TaskRepository(String fileName) {
        super(fileName);
        //file = new File(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateStorage();
    }

    private static TaskRepository instance = null;

    public static TaskRepository getInstance(){
        synchronized (TaskRepository.class) {
            if (instance == null) {
                instance = new TaskRepository();
            }
        }
        return instance;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.remove(task);
        }
        return null;
    }

    @Override
    protected void saveStorage() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        try{
            objectMapper.writeValue(file, storage);
            //objectMapper.writeValue(file, items);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void updateStorage() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        try{
            storage = objectMapper.readValue(file, HashMap.class);
            //items = objectMapper.readValue(file, List.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("File is impty");
        }
    }
}
