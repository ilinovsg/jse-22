package ru.ilinovsg.tm.repository;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.ilinovsg.tm.entity.Project;

import java.io.*;
import java.util.HashMap;

public class ProjectRepository extends AbstractRepository<Project>{
    private final File file = new File("project.xml");

    private ProjectRepository() {
        this("project.xml");
    }

    public ProjectRepository(String fileName) {
        super(fileName);
        //file = new File(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateStorage();
    }

    private static ProjectRepository instance = null;

    public static ProjectRepository getInstance(){
        synchronized (ProjectRepository.class) {
            if (instance == null) {
                instance = new ProjectRepository();
            }
        }
        return instance;
    }

    @Override
    /*protected void saveStorage() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        try{
            objectMapper.writeValue(file, storage);
            //objectMapper.writeValue(file, items);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
    protected void saveStorage() {
        XmlMapper xmlMapper = new XmlMapper();

        try{
             xmlMapper.writeValue(file, storage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    /*protected void updateStorage() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        try{
            storage = objectMapper.readValue(file, HashMap.class);
            //items = objectMapper.readValue(file, List.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("File is impty");
        }
    }*/
    protected void updateStorage() {
        XmlMapper xmlMapper = new XmlMapper();

        try{
            String xml = inputStreamToString(new FileInputStream(file));
            storage =  xmlMapper.readValue(xml, HashMap.class);
        } catch (IOException e) {
            System.out.println("File is empty");
        }
    }

    public static String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }

}
