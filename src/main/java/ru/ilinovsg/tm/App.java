package ru.ilinovsg.tm;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.enumerated.Role;
import ru.ilinovsg.tm.observer.*;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.service.*;
import ru.ilinovsg.tm.utils.hashMD5;

import java.io.*;

import static ru.ilinovsg.tm.constant.TerminalConst.PROJECT_FILE_NAME_JSON;
import static ru.ilinovsg.tm.constant.TerminalConst.TASK_FILE_NAME_JSON;

public class App {

    private static ProjectRepository projectRepository = ProjectRepository.getInstance();
    ProjectService projectService = ProjectService.getInstance();
    private static TaskRepository taskRepository = TaskRepository.getInstance();
    TaskService taskService = TaskService.getInstance();
    UserRepository userRepository = UserRepository.getInstance();
    ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    {
        projectRepository.create(new Project("PROJECT 3"));
        projectRepository.create(new Project("PROJECT 1"));
        projectRepository.create(new Project("PROJECT 2"));
        taskRepository.create(new Task("TASK 3"));
        taskRepository.create(new Task("TASK 1"));
        taskRepository.create(new Task("TASK 2"));

        userRepository.create("ADMIN", hashMD5.md5("123QWE"), "Ivan", "Ivanov", Role.Admin);
        userRepository.create("TEST", hashMD5.md5("123"), "Peter", "Petrov", Role.User);
    }

    public static void main(final String[] args) throws IOException {
        new App();
        displayWelcome();

        //writeProjectToFileJSON(PROJECT_FILE_NAME_JSON);
        //writeProjectToFileXML("project.xml");
        //writeTaskToFileJSON(TASK_FILE_NAME_JSON);

        Publisher publisher = new PublisherImpl();
        ListenerProjectImpl listenerProject = new  ListenerProjectImpl();
        ListernerTaskImpl listenerTask = new ListernerTaskImpl();
        ListenerSystemImpl listenerSystem = new ListenerSystemImpl();
        ListenerUserImpl listenerUser = new ListenerUserImpl();
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
        publisher.addListener(listenerSystem);
        publisher.addListener(listenerUser);
        publisher.start();
    }

    public static void writeProjectToFileJSON(String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName, false);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        for (final Project project : App.projectRepository.findAll()) {
            String prettyJson = "";
            try {
                prettyJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
                writer.write("project");
                writer.write(prettyJson);
                writer.append('\n');
                writer.flush();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeProjectToFileXML(String fileName) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        File file = new File("project.xml");
        //for (final Project project : App.projectRepository.findAll()) {
            try {
                xmlMapper.writeValue(new File("project.xml"), App.projectRepository.findAll());
            } catch (IOException  e) {
                e.printStackTrace();
            }
        //}
    }

    public static void writeTaskToFileJSON(String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName, false);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        for (final Task task : App.taskRepository.findAll()) {
            String prettyJson = "";
            try {
                prettyJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
                writer.write("task");
                writer.write(prettyJson);
                writer.append('\n');
                writer.flush();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    public static void displayWelcome() {
        System.out.println("***WELCOME TO TASK-MANAGER***");
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}
