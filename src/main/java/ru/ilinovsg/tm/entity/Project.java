package ru.ilinovsg.tm.entity;

import java.io.Serializable;

public class Project extends AbstractEntity implements Serializable {

    public Project() {
    }

    public Project(String name) {
        super(name);
    }

    public Project(Long id, String name, String description, Long userId) {
        super(id, name, description, userId);
    }

    public Project(String name, String description) { super(name, description);
    }
}
