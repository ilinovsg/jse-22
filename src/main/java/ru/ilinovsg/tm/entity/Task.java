package ru.ilinovsg.tm.entity;

import java.io.Serializable;

public class Task extends AbstractEntity implements Serializable {

    private Long projectId;

    public Task() {
    }

    public Task(String name) {
        super(name);
    }

    public Task(Long id, String name, String description, Long userId, Long projectId) {
        super(id, name, description, userId);
        this.projectId = projectId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }
}
