package ru.ilinovsg.tm.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.repository.AbstractRepository;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ProjectTaskService extends AbstractRepository {
    private final File file = new File(fileName);

    private ProjectTaskService() {
        this("project.xml");
    }

    public ProjectTaskService(String fileName) {
        super(fileName);
        //file = new File(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateStorage();
    }

    private static ProjectTaskService instance = null;

    public static ProjectTaskService getInstance(){
        synchronized (ProjectTaskService.class) {
            if (instance == null) {
                instance = new ProjectTaskService();
            }
        }
        return instance;
    }

    ProjectRepository projectRepository = ProjectRepository.getInstance();

    TaskRepository taskRepository = TaskRepository.getInstance();

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task removeTaskFromProject (final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject (final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final Project project;
        project = projectRepository.findById(projectId);
        final Task task = taskRepository.findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    public void removeTasksAndProject (final Long projectId) throws ProjectNotFoundException, TaskNotFoundException {
        final Project project;
        project = projectRepository.findById(projectId);
        taskRepository.removeByProjectId(projectId);
        projectRepository.removeById(projectId);
        return;
    }

    @Override
    protected void saveStorage() {
        XmlMapper xmlMapper = new XmlMapper();

        try{
            xmlMapper.writeValue(file, storage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void updateStorage() {
        XmlMapper xmlMapper = new XmlMapper();

        try{
            xmlMapper.readValue(file, HashMap.class);
        } catch (IOException e) {
            System.out.println("File is empty");
        }
    }

}
